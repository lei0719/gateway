package com.example.demo;


import com.netflix.loadbalancer.AbstractServerPredicate;
import com.netflix.loadbalancer.AvailabilityPredicate;
import com.netflix.loadbalancer.CompositePredicate;
import com.netflix.loadbalancer.PredicateBasedRule;

public class GrayLevelRule extends PredicateBasedRule {

    private final CompositePredicate predicate;

    public GrayLevelRule(GrayLevelPredicate  predicate) {
        this.predicate = CompositePredicate.withPredicates(predicate, new AvailabilityPredicate(this,null))
        .build();
    }

    public GrayLevelRule() {
		this(new GrayLevelPredicate(Const.GRAY_LEVEL_MARK)); 
	}

	@Override
    public AbstractServerPredicate getPredicate() {
        return this.predicate;
    }
  
}
