package com.example.demo;

import org.springframework.core.NamedThreadLocal;
 import org.springframework.web.server.ServerWebExchange;

public abstract class ServerWebExchangeContextHolder{

	private static final ThreadLocal<ServerWebExchange> serverWebExchangeHolder =
			new NamedThreadLocal<>("Request attributes");
	
	
	public static void setServerWebExchange(ServerWebExchange exchange) {
		serverWebExchangeHolder.set(exchange);
	}
	
	public static ServerWebExchange getServerWebExchange() {
		return serverWebExchangeHolder.get();
	}
	
	public static  void rest() {
		serverWebExchangeHolder.remove();
	}

}
