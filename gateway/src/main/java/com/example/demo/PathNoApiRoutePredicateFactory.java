package com.example.demo;

import java.util.function.Predicate;

import org.springframework.cloud.gateway.handler.predicate.PathRoutePredicateFactory;
import org.springframework.web.server.ServerWebExchange;

public class PathNoApiRoutePredicateFactory extends PathRoutePredicateFactory {

	@Override
	public Predicate<ServerWebExchange> apply(Config config) {
		Predicate<ServerWebExchange> predicate= super.apply(config);
		return exchange->{
			if(exchange.getRequest().getURI().getPath().startsWith("/api/")) {
				return false;
			}
			return predicate.test(exchange);
		};
	}
}
