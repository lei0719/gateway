package com.example.demo;

import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.RewritePathGatewayFilterFactory;
import org.springframework.http.server.reactive.ServerHttpRequest;

import static org.springframework.cloud.gateway.support.ServerWebExchangeUtils.GATEWAY_REQUEST_URL_ATTR;
import static org.springframework.cloud.gateway.support.ServerWebExchangeUtils.addOriginalRequestUrl;

 
public class RewritePathBuildGatewayFilterFactory extends RewritePathGatewayFilterFactory{
	 
	@Override
	public GatewayFilter apply(Config config) {
		String replacement = config.getReplacement().replace("$\\", "$");
		return (exchange, chain) -> {
			ServerHttpRequest req = exchange.getRequest();
			addOriginalRequestUrl(exchange, req.getURI());
			String path = req.getURI().getRawPath();
			String hostName = req.getHeaders().getHost().getHostName();
			String newPath = "/"+hostName+path.replaceAll(config.getRegexp(), replacement);

			ServerHttpRequest request = req.mutate().path(newPath).build();

			exchange.getAttributes().put(GATEWAY_REQUEST_URL_ATTR, request.getURI());
			return chain.filter(exchange.mutate().request(request).build());
		};
	}

}
