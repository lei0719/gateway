package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.reactive.ResourceHandlerRegistrationCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.Resource;
import org.springframework.web.reactive.config.ResourceHandlerRegistration;
import org.springframework.web.reactive.resource.PathResourceResolver;

import reactor.core.publisher.Mono;

@SpringBootApplication
public class H5Application   {

	public static void main(String[] args) {
		SpringApplication.run(H5Application.class, args);
	}

	public @Bean ResourceHandlerRegistrationCustomizer handlerRegistrationCustomizer(){
		return (ResourceHandlerRegistration registration) ->{
			registration.resourceChain(true).addResolver(new PathResourceResolver() {
				@Override
				protected Mono<Resource> getResource(String resourcePath, Resource location) {
	 				System.err.println(location+resourcePath);
					return super.getResource(resourcePath, location);
				}
				
			});
		};
	} 

}
