package com.example.demo;

 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(Abc.class)
public class ExampleApplication implements CommandLineRunner{

	public static void main(String[] args) throws InterruptedException {
		SpringApplication.run(ExampleApplication.class, args);
		
	}

	@Autowired Abc abc;
	@Override
	public void run(String... args) throws Exception {
		 
 	}
	

}
